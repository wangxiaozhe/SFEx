<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--引入easyui的样式 -->
<link rel="stylesheet"  type="text/css" href="${pageContext.request.contextPath}/themes/default/easyui.css">
<link rel="stylesheet"  type="text/css" href="${pageContext.request.contextPath}/themes/icon.css">
<!-- js文件 -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-lang-zh_CN.js"></script>
<title>Insert title here</title>
</head>
<script type="text/javascript">
/**
 * 左边部分下拉列表
 */
 $(function(){
		$('#methodTree').tree({    
			url:"${pageContext.request.contextPath }"+'/user/getAllMethods.action',
			queryParams:{"parentId":-1},
			onLoadSuccess:function(node, data){
				$('#methodTree').tree('expandAll');
			},
			onClick:function(node){
				if(node.attributes.url){
					if($("#mtabs").tabs('exists',node.text)){
						$("#mtabs").tabs('select',node.text)
					}else{
						var pageContent = "<iframe frameborder=0 src="+node.attributes.url+" style='width:100%;height:100%'>"
						$("#mtabs").tabs('add',{
							title:node.text,
							closable:true,
							fit:true,
							content:pageContent
						});
					}
				}
			}
		});
	})

</script>

<body  class="easyui-layout" style="margin:5px">
	<div data-options="region:'north',title:'物流管理系统',split:false,collapsible:false" style="height:120px;text-aligen:center">
	<h1>欢迎使用中软物流管理系统</h1>
	</div>
	<!-- 左边功能列表 -->
	<div data-options="region:'west',title:' ',split:true"
		style="width: 150px;">
		<ul id="methodTree">
		</ul>
	</div>
	<!-- 中间部分 -->
	<div data-options="region:'center'" style="padding:5px;background:#eee;">
		<div id="mtabs" class="easyui-tabs" style=";width:100%;height:100%">   
    		<div title="首页" data-options="fit:true" style="padding:20px;display:none">
				欢迎使用首页
    		</div>
		</div>
	</div>
	<div data-options="region:'south',split:false,collapsible:false,border:false" style="height:50px;">版权所有</div>
</body>
</html>