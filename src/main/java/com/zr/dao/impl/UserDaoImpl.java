package com.zr.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.junit.Test;
import org.springframework.stereotype.Repository;

import com.zr.dao.UserDao;
import com.zr.model.Function;
import com.zr.model.User;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Repository("userDao")
public class UserDaoImpl<T> extends BaseDaOImpl<T> implements UserDao<T>{

	
	@Override
	public JSONArray getMethodById(int parentid,int rid) {
		JSONArray js=new JSONArray();
		String sql="select * from function where fid in (select fid from role_function where rid=:rid) and parentid=:parentid ";
		SQLQuery query=this.getCurrentSession().createSQLQuery(sql).addEntity(Function.class);
		query.setInteger("rid", rid);
		query.setInteger("parentid", parentid);
		List<Function> functions=query.list();
		for(Function function:functions){
			JSONObject jObject=new JSONObject();
			jObject.put("id", function.getFid());
			jObject.put("text", function.getFname());
			jObject.put("state", function.getState());
			JSONObject attrobj=new JSONObject();
			attrobj.put("url",function.getFpath());
			jObject.put("attributes", attrobj);
			js.add(jObject);
		}
		return js;
	}

	@Override
	public JSONArray getAllMethodsById(int parentid,int rid) {
		JSONArray js=this.getMethodById(parentid,rid);
		for(int i=0;i<js.size();i++){
			JSONObject jsobj=js.getJSONObject(i);
			if("open".equals(jsobj.get("state"))){
				continue;
			}else{
				jsobj.put("children", getAllMethodsById(Integer.parseInt(jsobj.getString("id")),rid));
			}
		}
		return js;
	}

}
