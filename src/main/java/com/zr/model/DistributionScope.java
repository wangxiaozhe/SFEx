package com.zr.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="distributionscope")
public class DistributionScope {
	private int dsid;
	private String dsname;
	private Date dstime;
	private String dsfs;
	private double dsprice;
	private DistributionCenter dc;
	
	@Id
	@GeneratedValue
	public int getDsid() {
		return dsid;
	}
	public void setDsid(int dsid) {
		this.dsid = dsid;
	}
	public String getDsname() {
		return dsname;
	}
	public void setDsname(String dsname) {
		this.dsname = dsname;
	}
	public Date getDstime() {
		return dstime;
	}
	public void setDstime(Date dstime) {
		this.dstime = dstime;
	}
	public String getDsfs() {
		return dsfs;
	}
	public void setDsfs(String dsfs) {
		this.dsfs = dsfs;
	}
	public double getDsprice() {
		return dsprice;
	}
	public void setDsprice(double dsprice) {
		this.dsprice = dsprice;
	}
	
	@ManyToOne
	@JoinColumn(name="did")
	public DistributionCenter getDc() {
		return dc;
	}
	public void setDc(DistributionCenter dc) {
		this.dc = dc;
	}
	
}
